import React from 'react'

const NoMatch = () => {
  return (
    <div style={divStyle}>
      <h1 style={headerStyle}>404</h1>
      <p style={pStyle}>Page Not Found!</p>
    </div>
  )
}

const divStyle = {
  padding: '100px 0'
};
const headerStyle = {
  fontSize: '140px',
  fontWeight: '900',
  textAlign: 'center',
  color: '#bdbdbd',
  marginBottom: '-15px'
};
const pStyle = {
  fontSize: '14px',
  fontWeight: '700',
  textAlign: 'center',
  color: '#292929'
};

export default NoMatch
