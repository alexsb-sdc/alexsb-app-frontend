//React
import React, { Component } from 'react'

//Proptypes
import PropTypes from 'prop-types'

//Local-Files
import './MenuOverlay.css'

class MenuOverlay extends Component {
    render() {
        return (
            <div
                className={`overlay ${this.props.visible? "show" : "hidden"}`}
                id="menu-overlay"
                onClick={this.props.handleClick}>
            </div>
        )
    }
}

// PropTypes
MenuOverlay.propTypes = {
    visible: PropTypes.bool.isRequired
}

export default MenuOverlay