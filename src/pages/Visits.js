//React
import React, { Component } from 'react'

//Local-Files
import SearchPageHeader from '../components/headers/SearchPageHeader'

class Visits extends Component {
  pageDetails = {
    title: "Visits",
    headerText: "IEEE Visits",
    address: [
      {
        title: "Home",
        linkTo: '/'
      }
    ]
  }

  render() {
    return (
        <SearchPageHeader pageDetails={this.pageDetails}/>
    )
  }
}

export default Visits