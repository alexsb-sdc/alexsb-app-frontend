//React
import React, { Component } from 'react'

//React-Router-Dom
import { Link } from 'react-router-dom'

//Local-Files
import './LogoArea.css'

class LogoArea extends Component {
  render() {
    return (
        <div className="logo-area">
            <Link to="/"><img  src={require('./../../../../assets/logo.png')} alt="IEEE Logo"/></Link>
            <p>© 2019 IEEE Alex sb All Rights Reserved.</p>
        </div>
    )
  }
}

export default LogoArea