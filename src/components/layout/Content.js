//React
import React, { Component } from 'react'

class Content extends Component {
  render() {
    return (
        <main role="main" className="flex-shrink-0">
            {this.props.children}
        </main>
    )
  }
}

export default Content