import React, { Component } from 'react'

import {Container} from 'react-bootstrap';
import Location from './Location';

import './headers.css';

export default class SearchPageHeader extends Component {
  render() {
    return (
      <div className="search-page-header">
        <Container>
            <Location address={[{title:"Home", linkTo: "/"}]} title="Courses"/>
            <h1 className="page-big-title">IEEE Courses</h1>
        </Container>
      </div>
    )
  }
}
