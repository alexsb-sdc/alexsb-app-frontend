//React
import React, { Component } from 'react'

//Local-Files
import SearchPageHeader from '../components/headers/SearchPageHeader'

class Events extends Component {
  pageDetails = {
    title: "Events",
    headerText: "IEEE Events",
    address: [
      {
        title: "Home",
        linkTo: '/'
      }
    ]
  }

  render() {
    return (
        <SearchPageHeader pageDetails={this.pageDetails}/>
    )
  }
}

export default Events