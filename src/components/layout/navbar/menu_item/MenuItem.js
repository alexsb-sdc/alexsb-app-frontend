//React
import React, { Component } from 'react'

//Proptypes
import PropTypes from 'prop-types'

//React-Router-Dom
import { Link } from 'react-router-dom';

//Local-Files
import './MenuItem.css'

class MenuItem extends Component {
  render() {
    return (
        <div className="menu-item">
            <Link className="nav-link" to={`/${this.props.linkTo}`}>
            <div className={`menu-item-icon ${this.props.iconClass}`}></div>
            {this.props.title}</Link>
        </div>
    )
  }
}

// PropTypes
MenuItem.propTypes = {
  linkTo: PropTypes.string.isRequired,
  iconClass: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
}

export default MenuItem