//React
import React, { Component } from 'react'

//Proptypes
import PropTypes from 'prop-types'

//Local-Files
import './SocialMedia.css'

class SocialMedia extends Component {
  render() {
    return (
      <div className={`social-media-area ${this.props.className}`}>
            <h6 className="title-small">Social media</h6>
            <div>
                <a href="https://www.facebook.com" rel="noopener noreferrer" target="_blank">
                  <i className="fab fa-facebook-square"></i>
                </a>
                <a href="https://www.facebook.com" rel="noopener noreferrer" target="_blank">
                  <i className="fab fa-twitter-square"></i>
                </a>
            </div>
      </div>
    )
  }
}

// PropTypes
SocialMedia.propTypes = {
  className: PropTypes.string
}

export default SocialMedia