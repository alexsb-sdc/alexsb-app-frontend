//React
import React, { Component } from 'react'

//React-Router-Dom
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

//Local-Files
import Home from './pages/Home'
import Contact from './pages/Contact'
import About from './pages/About'
import NoMatch from './pages/NoMatch'
import Courses from './pages/Courses'

import { Layout } from './components/layout/Layout'

import './App.css'

class App extends Component {
  render() {
    return (
      <Router>
        <Layout>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/about" component={About} />
            <Route path="/contact" component={Contact} />
            <Route path="/courses" component={Courses} />
            <Route component={NoMatch} />
          </Switch>
        </Layout>
      </Router>
    )
  }
}

export default App
