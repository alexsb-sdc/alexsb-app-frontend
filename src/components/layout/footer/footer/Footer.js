//React
import React, { Component } from 'react'

//React-Bootstrap
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

//React-Router-Dom
import { Link } from 'react-router-dom'

//Local-Files
import SiteMap from './../site_map/SiteMap'
import SocialMedia from './../social_media/SocialMedia'
import LogoArea from './../logo_area/LogoArea';
import './Footer.css'

class Footer extends Component {
  render() {
    return (
        <footer className="footer mt-auto py-3">
          <Container>
            <Row>
              <Col lg={{span: 4, order: 3 }} md={6} sm={{span: 7, order: 2}} >
                <h6 className="title-small stay-visible">Have a question or feedback ?</h6>
                <Link to="/contact" className="btn-layout btn btn-primary">Contact us</Link>
                <Link to="/about" className="btn-layout btn btn-light">About IEEE</Link>
                <SocialMedia className="d-lg-none" />
              </Col>
              <Col className="d-none d-lg-block" lg={{ order: 4 }}>
                <SocialMedia />
              </Col>
              <Col lg={{ order: 2 }} md={6} sm={{span: 5, order: 1}}>
                <SiteMap />
              </Col>
              <Col lg={{span: 3, order: 1 }} sm={{span: 12, order: 3}}>
                <LogoArea />
              </Col>
            </Row>
          </Container>
        </footer>
    )
  }
}

export default Footer
