//React
import React, { Component } from 'react'

//Proptypes
import PropTypes from 'prop-types'

//React-Router-Dom
import { Link } from 'react-router-dom'
import './FooterLink.css'

class FooterLink extends Component {
  render() {
    return (
        <Link className="footer-link" to={`/${this.props.linkTo}`}>
          <div className={`menu-item-icon ${this.props.iconClass}`}></div>
          {this.props.title}
        </Link>
    )
  }
}

// PropTypes
FooterLink.propTypes = {
  linkTo: PropTypes.string.isRequired,
  iconClass: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
}

export default FooterLink