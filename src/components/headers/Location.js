import React, { Component } from 'react'

import Breadcrumb from 'react-bootstrap/Breadcrumb'
import LocationItem from './LocationItem'

class Location extends Component {
  render() {
    return (
      <Breadcrumb>
        {
          this.props.address.map((page) => (
            <LocationItem key={page.title} linkTo={page.linkTo}>{page.title}</LocationItem>
          ))
        }
        <Breadcrumb.Item active>{this.props.title}</Breadcrumb.Item>
      </Breadcrumb>
    )
  }
}

export default Location;
