//React
import React, { Component } from 'react'

//Local-Files
import FooterLink from '../footer_link/FooterLink'
import './SiteMap.css'

class SiteMap extends Component {
  render() {
    return (
        <ul className="site-map">
            <h6 className="title-small">Sitemap</h6>
            <li><FooterLink linkTo="courses" title="Courses" iconClass="courses-icon" /></li>  
            <li><FooterLink linkTo="events" title="Events" iconClass="events-icon" /></li>
            <li><FooterLink linkTo="visits" title="Visits" iconClass="visits-icon" /></li>
            <li><FooterLink linkTo="blog" title="Blog" iconClass="blogs-icon" /></li>
        </ul>
    )
  }
}

export default SiteMap