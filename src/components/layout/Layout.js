//React
import React from 'react';

//Local-Files
import Menu from './navbar/menu/Menu';
import Footer from './footer/footer/Footer';
import Content from './Content';

import './../../assets/css/helpers.css';
import './../../assets/css/buttons.css';
import './../../assets/css/icons.css';

export const Layout = (props) => {
    return (
        <React.Fragment>
            <Menu />
            <Content>
                {props.children}
            </Content>
            <Footer />
        </React.Fragment>
    )
}