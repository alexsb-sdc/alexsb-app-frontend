//React
import React, { Component } from 'react'

//React-Bootstrap
import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

//React-Router-Dom
import { Link } from 'react-router-dom'

//Local-Files
import MenuItem from './../menu_item/MenuItem'
import MenuOverlay from './../menu_overlay/MenuOverlay';
import './Menu.css'

class Menu extends Component {
    state = {
        opened: false
    }

    handleToggle = (opened) => this.setState({opened})
    
    
    handleClick = () => {
        if(this.state.opened)
            this.toggler.click();
    }

    render() {
        return (
            <React.Fragment>
                <MenuOverlay visible={this.state.opened} handleClick={this.handleClick}/>
                <Navbar bg="light" onToggle={this.handleToggle} expand="sm" fixed="top">
                    <Container>
                        <Navbar.Brand>
                            <Link to="/"><img  src={require('./../../../../assets/logo.png')} alt="IEEE Logo"/></Link>
                        </Navbar.Brand>
                        <Navbar.Toggle ref={toggler_btn => this.toggler = toggler_btn} aria-controls="basic-navbar-nav"><i className="fas fa-bars"></i></Navbar.Toggle>
                        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-md-center">
                            <Nav className="ml-auto">
                                <MenuItem linkTo="courses" title="Courses" iconClass="courses-icon"/>
                                <MenuItem linkTo="events" title="Events" iconClass="events-icon"/>
                                <MenuItem linkTo="visits" title="Visits" iconClass="visits-icon"/>
                                <MenuItem linkTo="blog" title="Blog" iconClass="blogs-icon"/>
                            </Nav>
                            <h6 className="d-block d-sm-none title-small menu-title">Have a question or feedback ?</h6>
                            <Link to="/contact" className="ml-auto btn-layout btn btn-primary">Contact us</Link>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            </React.Fragment>
        )
    }
}

export default Menu