import React, { Component } from 'react'

import {Link} from 'react-router-dom'

export default class LocationItem extends Component {
  render() {
    return (
      <li className="breadcrumb-item">
        <Link to={this.props.linkTo}>{this.props.children}</Link>
      </li>
    )
  }
}
